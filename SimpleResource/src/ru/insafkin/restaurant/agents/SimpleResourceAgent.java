package ru.insafkin.restaurant.agents;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import ru.insafkin.restaurant.behaviours.HandleSimpleResourceBehaviour;
import ru.insafkin.restaurant.behaviours.RegisterInDFBehaviour;
import ru.insafkin.restaurant.ontologies.RestaurantOntology;
import ru.insafkin.restaurant.ontologies.simpleResource.SimpleResource;

/**
 * Created by saferif on 17.04.16.
 */
public class SimpleResourceAgent extends Agent {
    private SimpleResource resource;

    private Codec codec = new SLCodec();
    private Ontology ontology = RestaurantOntology.getInstance();

    @Override
    protected void setup() {
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        createResource(getArguments());

        addBehaviour(new RegisterInDFBehaviour(this, resource.getId(), "MyRestaurant"));
        addBehaviour(new HandleSimpleResourceBehaviour(this, resource));
    }

    private void createResource(Object[] arguments) {
        if (arguments.length != 3) {
            throw new IllegalArgumentException("3 arguments expected in SimpleResourceAgent");
        }

        String resourceId = (String) arguments[0];
        String resourceDisplayName = (String) arguments[1];
        double resourceInitialAmount = (Double) arguments[2];

        resource = new SimpleResource(resourceId, resourceDisplayName);
        resource.add(resourceInitialAmount);
    }
}
