package ru.insafkin.restaurant.behaviours;

import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import ru.insafkin.restaurant.ontologies.simpleResource.ClaimSimpleResource;
import ru.insafkin.restaurant.ontologies.simpleResource.SimpleResource;

/**
 * Created by saferif on 18.04.16.
 */
public class ClaimSimpleResourceBehaviour extends OneShotBehaviour {
    private SimpleResource resource;
    private Action message;

    public ClaimSimpleResourceBehaviour(Agent agent, SimpleResource resource, Action message) {
        super(agent);
        this.resource = resource;
        this.message = message;
    }

    @Override
    public void action() {
        resource.claimReserved(message.getActor(), ((ClaimSimpleResource) message.getAction()).getAmount());
    }
}
