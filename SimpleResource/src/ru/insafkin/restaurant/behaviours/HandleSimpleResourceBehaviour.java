package ru.insafkin.restaurant.behaviours;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import ru.insafkin.restaurant.ontologies.simpleResource.*;

/**
 * Created by saferif on 18.04.16.
 */
public class HandleSimpleResourceBehaviour extends CyclicBehaviour {
    private SimpleResource resource;

    public HandleSimpleResourceBehaviour(Agent agent, SimpleResource resource) {
        super(agent);
        this.resource = resource;
    }

    @Override
    public void action() {
        ACLMessage message = myAgent.blockingReceive();
        try {
            ContentElement content = myAgent.getContentManager().extractContent(message);
            Concept action = ((Action)content).getAction();

            if (action instanceof ReserveSimpleResource) {
                myAgent.addBehaviour(new ReserveSimpleResourceBehaviour(myAgent, resource, (Action) content));
            } else if (action instanceof ClaimSimpleResource) {
                myAgent.addBehaviour(new ClaimSimpleResourceBehaviour(myAgent, resource, (Action) content));
            } else if (action instanceof CancelSimpleResource) {
                myAgent.addBehaviour(new CancelSimpleResourceBehaviour(myAgent, resource, (Action) content));
            } else if (action instanceof AddSimpleResource) {
                myAgent.addBehaviour(new AddSimpleResourceBehaviour(myAgent, resource, (Action) content));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
