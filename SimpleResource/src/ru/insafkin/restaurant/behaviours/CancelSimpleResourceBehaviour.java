package ru.insafkin.restaurant.behaviours;

import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import ru.insafkin.restaurant.ontologies.simpleResource.CancelSimpleResource;
import ru.insafkin.restaurant.ontologies.simpleResource.SimpleResource;

/**
 * Created by saferif on 18.04.16.
 */
public class CancelSimpleResourceBehaviour extends OneShotBehaviour {
    private SimpleResource resource;
    private Action message;

    public CancelSimpleResourceBehaviour(Agent agent, SimpleResource resource, Action message) {
        super(agent);
        this.resource = resource;
        this.message = message;
    }

    @Override
    public void action() {
        resource.cancel(message.getActor(), ((CancelSimpleResource) message.getAction()).getAmount());
    }
}
