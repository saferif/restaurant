package ru.insafkin.restaurant.agents;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import ru.insafkin.restaurant.behaviours.DoTasksBehaviour;
import ru.insafkin.restaurant.behaviours.HandleProcessBehaviour;
import ru.insafkin.restaurant.behaviours.RegisterInDFBehaviour;
import ru.insafkin.restaurant.ontologies.RestaurantOntology;
import ru.insafkin.restaurant.ontologies.process.ProcessingTask;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by saferif on 22.05.16.
 */
public class ProcessAgent extends Agent {
    private Codec codec = new SLCodec();
    private Ontology ontology = RestaurantOntology.getInstance();

    private String id;
    private int duration;
    private final LinkedList<InternalTask> queue = new LinkedList<>();

    private int counter = 0;

    @Override
    protected void setup() {
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        createProcess(getArguments());

        addBehaviour(new RegisterInDFBehaviour(this, id, "MyRestaurant"));
        addBehaviour(new MainBehaviour(this));
    }

    private void createProcess(Object[] arguments) {
        if (arguments.length != 2) {
            throw new IllegalArgumentException("2 arguments expected in ProcessAgent");
        }

        id = (String) arguments[0];
        duration = (Integer) arguments[1];
    }

    public ProcessingTask createTask(AID source) {
        synchronized (queue) {
            String id = String.valueOf(counter++);
            queue.add(new InternalTask(id ,source));
            return new ProcessingTask(id, false, queue.size() - 1, queue.size() * duration);
        }
    }

    public ProcessingTask findTask(String id) {
        synchronized (queue) {
            int index = -1;
            Iterator<InternalTask> it = queue.iterator();
            for (int i = 0; i < queue.size(); i++) {
                if (it.next().getId().equals(id)) {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                return new ProcessingTask(id, false, index, (index + 1) * duration);
            } else {
                return new ProcessingTask(id, false, -1, -1);
            }
        }
    }

    public InternalTask getTask() {
        synchronized (queue) {
            if (queue.isEmpty()) {
                return null;
            }
            InternalTask task = queue.get(0);
            queue.remove(0);
            return task;
        }
    }

    public int getDuration() {
        return duration;
    }

    public static class InternalTask {
        private String id;
        private AID source;

        InternalTask(String id, AID source) {
            this.id = id;
            this.source = source;
        }

        public String getId() {
            return id;
        }

        public AID getSource() {
            return source;
        }
    }

    private static class MainBehaviour extends ParallelBehaviour {
        MainBehaviour(Agent agent) {
            super(agent, ParallelBehaviour.WHEN_ALL);
            addSubBehaviour(new HandleProcessBehaviour(agent));
            addSubBehaviour(new DoTasksBehaviour(agent));
        }
    }
}
