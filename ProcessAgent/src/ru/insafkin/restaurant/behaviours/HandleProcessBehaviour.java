package ru.insafkin.restaurant.behaviours;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import ru.insafkin.restaurant.ontologies.process.CreateTask;
import ru.insafkin.restaurant.ontologies.process.FindTask;

/**
 * Created by saferif on 22.05.16.
 */
public class HandleProcessBehaviour extends CyclicBehaviour {

    public HandleProcessBehaviour(Agent agent) {
        super(agent);
    }

    @Override
    public void action() {
        ACLMessage message = myAgent.blockingReceive();
        try {
            ContentElement content = myAgent.getContentManager().extractContent(message);
            Concept action = ((Action)content).getAction();

            if (action instanceof CreateTask) {
                myAgent.addBehaviour(new CreateTaskBehaviour(myAgent, message));
            } else if (action instanceof FindTask) {
                myAgent.addBehaviour(new FindTaskBehaviour(myAgent, message));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
