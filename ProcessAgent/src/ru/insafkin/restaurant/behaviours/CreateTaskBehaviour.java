package ru.insafkin.restaurant.behaviours;

import jade.content.ContentElement;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import ru.insafkin.restaurant.agents.ProcessAgent;
import ru.insafkin.restaurant.ontologies.process.ProcessingTask;

/**
 * Created by saferif on 22.05.16.
 */
public class CreateTaskBehaviour extends OneShotBehaviour {
    private ACLMessage request;

    public CreateTaskBehaviour(Agent agent, ACLMessage request) {
        super(agent);
        this.request = request;
    }
    @Override
    public void action() {
        try {
            ProcessingTask task = ((ProcessAgent) myAgent).createTask(request.getSender());
            ContentElement content = myAgent.getContentManager().extractContent(request);
            Result result = new Result((Action) content, task);
            ACLMessage reply = request.createReply();
            reply.setPerformative(ACLMessage.INFORM);
            myAgent.getContentManager().fillContent(reply, result);
            myAgent.send(reply);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
