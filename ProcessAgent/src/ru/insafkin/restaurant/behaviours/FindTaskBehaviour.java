package ru.insafkin.restaurant.behaviours;

import jade.content.ContentElement;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import ru.insafkin.restaurant.agents.ProcessAgent;
import ru.insafkin.restaurant.ontologies.process.FindTask;
import ru.insafkin.restaurant.ontologies.process.ProcessingTask;

/**
 * Created by saferif on 22.05.16.
 */
public class FindTaskBehaviour extends OneShotBehaviour {
    private ACLMessage request;

    public FindTaskBehaviour(Agent agent, ACLMessage request) {
        super(agent);
        this.request = request;
    }

    @Override
    public void action() {
        try {
            ContentElement content = myAgent.getContentManager().extractContent(request);
            FindTask findTask = (FindTask)((Action) content).getAction();
            ProcessingTask task = ((ProcessAgent) myAgent).findTask(findTask.getId());
            Result result = new Result((Action) content, task);
            ACLMessage reply = request.createReply();
            reply.setPerformative(ACLMessage.INFORM);
            myAgent.getContentManager().fillContent(reply, result);
            myAgent.send(reply);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
