package ru.insafkin.restaurant.behaviours;

import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import ru.insafkin.restaurant.agents.ProcessAgent;
import ru.insafkin.restaurant.ontologies.RestaurantOntology;
import ru.insafkin.restaurant.ontologies.process.ProcessingTask;

/**
 * Created by saferif on 22.05.16.
 */
public class DoTasksBehaviour extends CyclicBehaviour {
    public DoTasksBehaviour(Agent agent) {
        super(agent);
    }

    @Override
    public void action() {
        ProcessAgent agent = (ProcessAgent) myAgent;
        ProcessAgent.InternalTask task = agent.getTask();
        if (task != null) {
            block(agent.getDuration());

            ACLMessage message = new ACLMessage(ACLMessage.INFORM);
            message.setLanguage(myAgent.getContentManager().getLanguageNames()[0]);
            message.setOntology(RestaurantOntology.ONTOLOGY_NAME);

            try {
                myAgent.getContentManager().fillContent(message, new Action(task.getSource(), new ProcessingTask(task.getId(), true, 0, 0)));
                message.addReceiver(task.getSource());
                myAgent.send(message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
