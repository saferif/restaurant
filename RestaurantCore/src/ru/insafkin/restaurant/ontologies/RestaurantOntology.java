package ru.insafkin.restaurant.ontologies;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import ru.insafkin.restaurant.ontologies.process.ProcessingTask;
import ru.insafkin.restaurant.ontologies.simpleResource.SimpleResource;

/**
 * Created by saferif on 17.04.16.
 */
public class RestaurantOntology extends Ontology {
    public static String ONTOLOGY_NAME = "Restaurant-Ontology";
    private static Ontology instance = new RestaurantOntology();
    public static Ontology getInstance() {
        return instance;
    }

    private RestaurantOntology() {
        super(ONTOLOGY_NAME, BasicOntology.getInstance());

        try {
            SimpleResource.registerOntology(this);
            ProcessingTask.registerOntology(this);
        } catch (OntologyException ex) {
            throw new RuntimeException(ex);
        }
    }
}
