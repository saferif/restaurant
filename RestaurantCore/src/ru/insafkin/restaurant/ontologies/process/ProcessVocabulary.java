package ru.insafkin.restaurant.ontologies.process;

/**
 * Created by saferif on 22.05.16.
 */
public interface ProcessVocabulary {
    String PROCESSING_TASK = "ProcessingTask";
    String CREATE_TASK = "CreateTask";
    String FIND_TASK = "FindTask";

    String ID = "id";
    String DONE = "done";
    String PLACE_IN_QUEUE = "place-in-queue";
    String ESTIMATED_FINISH_TIME = "estimated-finish-time";
}
