package ru.insafkin.restaurant.ontologies.process;

import jade.content.AgentAction;

/**
 * Created by saferif on 22.05.16.
 */
public class FindTask implements AgentAction {
    private String id;

    public FindTask(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
