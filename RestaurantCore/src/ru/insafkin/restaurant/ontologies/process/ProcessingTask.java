package ru.insafkin.restaurant.ontologies.process;

import jade.content.Concept;
import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PrimitiveSchema;

/**
 * Created by saferif on 22.05.16.
 */
public class ProcessingTask implements Concept {
    private String id;
    private boolean done;
    private int placeInQueue;
    private int estimatedFinishTime;

    public static void registerOntology(Ontology ontology) throws OntologyException {
        ConceptSchema cs = new ConceptSchema(ProcessVocabulary.PROCESSING_TASK);
        ontology.add(cs, ProcessingTask.class);
        cs.add(ProcessVocabulary.ID, (PrimitiveSchema) ontology.getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
        cs.add(ProcessVocabulary.DONE, (PrimitiveSchema) ontology.getSchema(BasicOntology.BOOLEAN), ObjectSchema.MANDATORY);
        cs.add(ProcessVocabulary.PLACE_IN_QUEUE, (PrimitiveSchema) ontology.getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
        cs.add(ProcessVocabulary.ESTIMATED_FINISH_TIME, (PrimitiveSchema) ontology.getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);

        AgentActionSchema as = new AgentActionSchema(ProcessVocabulary.CREATE_TASK);
        ontology.add(as, CreateTask.class);

        ontology.add(as = new AgentActionSchema(ProcessVocabulary.FIND_TASK), FindTask.class);
        as.add(ProcessVocabulary.ID, (PrimitiveSchema) ontology.getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
    }

    public ProcessingTask(String id, boolean done, int placeInQueue, int estimatedFinishTime) {
        this.id = id;
        this.done = done;
        this.placeInQueue = placeInQueue;
        this.estimatedFinishTime = estimatedFinishTime;
    }

    public String getId() {
        return id;
    }

    public boolean isDone() {
        return done;
    }

    public int getPlaceInQueue() {
        return placeInQueue;
    }

    public int getEstimatedFinishTime() {
        return estimatedFinishTime;
    }
}
