package ru.insafkin.restaurant.ontologies.simpleResource;

/**
 * Created by saferif on 17.04.16.
 */
public interface SimpleResourceVocabulary {
    // Fields
    String AMOUNT = "amount";
    String ID = "id";
    String DISPLAY_NAME = "display-name";
    String AMOUNT_LEFT = "amount-left";

    // Classes
    String SIMPLE_RESOURCE = "SimpleResource";
    String RESERVE_RESOURCE = "ReserveSimpleResource";
    String CANCEL_RESERVATION = "CancelSimpleResource";
    String CLAIM_RESERVED = "ClaimSimpleResource";
    String ADD_RESOURCE = "AddSimpleResource";
}
