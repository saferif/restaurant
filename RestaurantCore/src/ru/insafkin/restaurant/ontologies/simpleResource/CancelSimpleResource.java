package ru.insafkin.restaurant.ontologies.simpleResource;

import jade.content.AgentAction;

/**
 * Created by saferif on 17.04.16.
 */
public class CancelSimpleResource implements AgentAction {
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
