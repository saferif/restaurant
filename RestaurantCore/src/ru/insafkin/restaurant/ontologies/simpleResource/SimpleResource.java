package ru.insafkin.restaurant.ontologies.simpleResource;

import jade.content.Concept;
import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PrimitiveSchema;
import jade.core.AID;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saferif on 17.04.16.
 */
public class SimpleResource implements Concept {
    private String id;
    private String displayName;
    private double amountLeft;
    private Map<AID, Double> reserved;

    public static void registerOntology(Ontology ontology) throws OntologyException {
        ConceptSchema cs = new ConceptSchema(SimpleResourceVocabulary.SIMPLE_RESOURCE);
        ontology.add(cs, SimpleResource.class);
        cs.add(SimpleResourceVocabulary.ID, (PrimitiveSchema) ontology.getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
        cs.add(SimpleResourceVocabulary.DISPLAY_NAME, (PrimitiveSchema) ontology.getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
        cs.add(SimpleResourceVocabulary.AMOUNT_LEFT, (PrimitiveSchema) ontology.getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

        AgentActionSchema as = new AgentActionSchema(SimpleResourceVocabulary.RESERVE_RESOURCE);
        ontology.add(as, ReserveSimpleResource.class);
        as.add(SimpleResourceVocabulary.AMOUNT, (PrimitiveSchema) ontology.getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

        ontology.add(as = new AgentActionSchema(SimpleResourceVocabulary.CANCEL_RESERVATION), CancelSimpleResource.class);
        as.add(SimpleResourceVocabulary.AMOUNT, (PrimitiveSchema) ontology.getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

        ontology.add(as = new AgentActionSchema(SimpleResourceVocabulary.CLAIM_RESERVED), ClaimSimpleResource.class);
        as.add(SimpleResourceVocabulary.AMOUNT, (PrimitiveSchema) ontology.getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

        ontology.add(as = new AgentActionSchema(SimpleResourceVocabulary.ADD_RESOURCE), AddSimpleResource.class);
        as.add(SimpleResourceVocabulary.AMOUNT, (PrimitiveSchema) ontology.getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);
    }

    public SimpleResource(String id, String displayName) {
        this.id = id;
        this.displayName = displayName;
        this.reserved = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public double getAmountLeft() {
        return amountLeft;
    }

    public synchronized boolean reserve(AID aid, double amount) {
        if (amount > amountLeft) {
            return false;
        }

        double alreadyReserved = 0;
        if (reserved.containsKey(aid)) {
            alreadyReserved = reserved.get(aid);
        }

        amountLeft -= amount;
        reserved.put(aid, alreadyReserved + amount);

        return true;
    }

    public synchronized boolean claimReserved(AID aid, double amount) {
        if (!reserved.containsKey(aid)) {
            return false;
        }
        double reservedAmount = reserved.get(aid);
        if (amount > reservedAmount) {
            return false;
        }
        reserved.put(aid, reservedAmount - amount);
        return true;
    }

    public synchronized boolean cancel(AID aid, double amount) {
        if (!reserved.containsKey(aid)) {
            return false;
        }
        double reservedAmount = reserved.get(aid);
        if (amount > reservedAmount) {
            return false;
        }
        reserved.put(aid, reservedAmount - amount);
        amountLeft += amount;
        return true;
    }

    public synchronized void add(double amount) {
        amountLeft += amount;
    }
}
